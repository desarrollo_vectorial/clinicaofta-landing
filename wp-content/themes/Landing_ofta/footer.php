<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="col-md-12">
                    <a href="#" class="wow bounce btn btn-lg btn-footer">DÉJANOS TUS DATOS</a>
                </div>
                <div class="col-md-6 email">
                    <p>Email: <span>internacional@clinicaofta.com</span></p>
                </div>
                <div class="col-md-6 phone">
                    <p><i class="fa fa-whatsapp"></i> Cel: <a href="https://api.whatsapp.com/send?phone=573105950080&text=I'm%20interested%20in%20your%20services%20(Estoy%20interesado%20en%20sus%20servicios)" target="_blank">+57 3105950080</a></p>
                </div>
            </div>
        </div>
    </div>
    <div class="copy text-center">
        <p>Copyright todos los derechos reservados - Clínica de Oftalmología de Cali 2018 </p>
    </div>
</footer>

<script src="<?php bloginfo('template_url') ?>/assets/js/my-functions.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/wow.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>