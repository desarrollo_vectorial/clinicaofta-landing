<?php

/* Template Name: Full Width */

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
		<!-- AGREGA TUS ESTILOS -->
        <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico"/>
        <!-- AGREGA TUS SCRIPTS -->
        <script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
	 <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>

<?php
if (have_posts()) {
    while (have_posts()) {
        the_post();
        echo the_content();
    }
}

?>

</footer>

<script src="<?php bloginfo('template_url') ?>/assets/js/my-functions.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url') ?>/assets/js/wow.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>