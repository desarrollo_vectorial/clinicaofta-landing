<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?> <?php bloginfo('description'); ?></title>
		<!-- AGREGA TUS ESTILOS -->
        <link href="https://fonts.googleapis.com/css?family=Oxygen:300,400,700" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/assets/css/main.css" rel="stylesheet" type="text/css" />
        <link href="<?php bloginfo('template_url') ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url') ?>/assets/images/favicon.ico"/>
        <!-- AGREGA TUS SCRIPTS -->
        <script src="<?php bloginfo('template_url') ?>/assets/js/jquery.min.js"></script>
	 <?php
    wp_enqueue_script('jquery');
    wp_head();
    ?>
</head>
<body <?php body_class(); ?>>
<?php
// VARS ACF HEADER
$facebook = get_field('facebook', 'option');
$youtube = get_field('youtube', 'option');
$instagram = get_field('instagram', 'option');
?>
<header>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?php bloginfo('template_url') ?>/assets/images/clinica-de-oftalmologia-de-cali.jpg" alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div class="rrss">
                    <ul>
                        <li><a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $youtube; ?>" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a href="<?php echo $instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                    </ul>
                </div>
                <!--<div class="translate">
                    <ul>
                        <li>en</li>
                        <li>es</li>
                    </ul>
                </div>-->
                <?php
                wp_nav_menu(array(
                    'container' => false,
                    'items_wrap' => '<ul  class="nav navbar-nav navbar-right">%3$s</ul>',
                    'theme_location' => 'menu',
                ));
                ?>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</header>