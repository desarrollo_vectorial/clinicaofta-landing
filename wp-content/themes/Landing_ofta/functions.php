<?php

/**
 * Require functions on widgets, register_post_type and menus
 *
 * use require
**/

require TEMPLATEPATH . '/requires/menus/menus.php';
//require TEMPLATEPATH . '/requires/widgets/logos.php';
//require TEMPLATEPATH . '/requires/register_post_type/programas.php';

//JQ PARA SLIDER

function insert_jquery(){
    wp_enqueue_script('jquery', false, array(), false, false);
}
add_filter('wp_enqueue_scripts','insert_jquery',1);

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for plugin Theme
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
require_once dirname( __FILE__ ) . '/plugins_activation.php';

add_action( 'tgmpa_register', 'Theme_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function Theme_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
	$plugins = array(

		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'Advanced Custom Fields Pro', // The plugin name.
			'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
			'source'             => dirname( __FILE__ ) . '/requires/plugins/advanced-custom-fields-pro.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Contact Form 7 To Database', // The plugin name.
			'slug'               => 'contact-form-7-to-database-extension', // The plugin slug (typically the folder name).
			'source'             => dirname( __FILE__ ) . '/requires/plugins/contact-form-7-to-database-extension.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),
		array(
			'name'               => 'Revolution Slider', // The plugin name.
			'slug'               => 'revslider', // The plugin slug (typically the folder name).
			'source'             => dirname( __FILE__ ) . '/requires/plugins/revslider.zip', // The plugin source.
			'required'           => false, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		//array(
			//'name'         => 'TGM New Media Plugin', // The plugin name.
			//'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
			//'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
			//'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			//'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
		//),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		//array(
			//'name'      => 'Adminbar Link Comments to Pending',
			//'slug'      => 'adminbar-link-comments-to-pending',
			//'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		//),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => true,
		),
		array(
			'name'      => 'Wordfence Security',
			'slug'      => 'wordfence',
			'required'  => true,
		),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'Theme',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'Theme' ),
			'menu_title'                      => __( 'Install Plugins', 'Theme' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'Theme' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'Theme' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'Theme' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'Theme'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'Theme'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'Theme'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'Theme'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'Theme'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'Theme'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'Theme'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'Theme'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'Theme'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'Theme' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'Theme' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'Theme' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'Theme' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'Theme' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'Theme' ),
			'dismiss'                         => __( 'Dismiss this notice', 'Theme' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'Theme' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'Theme' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}

// Cambiar el pie de pagina del panel de Administración

function change_footer_admin() {  
    echo '&copy;2015 Copyright VECTORIAL. Todos los derechos reservados - Web creada por <a href="http://www.vectorial.co" target="_blank">Vectorial</a>';  
}  
add_filter('admin_footer_text', 'change_footer_admin');

// Widget para el Dashboard
 
    function custom_dashboard_widget() { ?>
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo-retina.png" style="width: 100%;max-width: 200px;margin: auto;display: block;"/>     
            <h1>¡Hola! Bienvenido al Dashboard de Vectorial Themes</h1>
            <p>Recomendaciones</p>
            <ol>
            	<li>No olviedes activar los plugins necesarios: (Plugins/Install Plugins)</li>
            	<li>Los estilos se encuentran en: (assets/css)</li>
            	<li>Para más información puedes leer el archivo Instrucciones dentro del tema</li>
            </ol>
    <?php } 
    
    add_action( 'wp_dashboard_setup', 'my_dashboard_setup_function' );
 
function my_dashboard_setup_function() {
    add_meta_box( 'my_dashboard_widget', 'Si puedes leer esto estás en una página desarrollada por Vectorial', 'custom_dashboard_widget', 'dashboard', 'normal', 'high' );
}

// activar post thumbnails
add_theme_support( 'post-thumbnails' );

function excerptPages () {
    add_post_type_support('page', 'excerpt');
}

// eliminar span contact-form 7
add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    return $content;
});

//THEME OPTIOS

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Opciones Principales',
        'menu_title'	=> 'Opciones del tema',
        'position'	=> '2.1',
        'icon_url' => 'dashicons-hammer',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Vertical Tabs',
        'menu_title'	=> 'Servicios',
        'parent_slug'	=> 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Ruta de atención',
        'menu_title'	=> 'Ruta de atención',
        'parent_slug'	=> 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Por qué escogernos',
        'menu_title'	=> 'Por qué escogernos',
        'parent_slug'	=> 'theme-general-settings',
    ));
    acf_add_options_sub_page(array(
        'page_title' 	=> 'Historias de éxito',
        'menu_title'	=> 'Historias de éxito',
        'parent_slug'	=> 'theme-general-settings',
    ));
}


//basic shell construction
function bsc_collaps( $atts, $content = null ) {
    extract(shortcode_atts( array(
        'title' => '', //topic title should be unique
        'id' => '', //topic ID should be unique
    ), $atts));
    //$clean_title = strtolower(urlencode($title)); //cleansing the title
    //$clean_title = preg_replace('/[^a-z0-9]+/i', '_', $title); //a bit more cleaning
    $html = '<h3>' . $title  . '</h3>'; //might as well use the title
    $html .= '<div id="fi-cite">';//special id in case we need to do some targeted CSS
    $html .= '<div class="panel-group" id="' . $id . '" role="tablist" aria-multiselectable="true">';
    $html .= do_shortcode($content);//this little bit it what
    $html .=  '</div></div>';//closes things out
    return  $html;
}
add_shortcode( 'collaps', 'bsc_collaps' );

//each item
function bsc_item( $atts, $content = null ) {
    global $post;
    $url = get_permalink($post);//gets our base URL to send the email link
    extract(shortcode_atts( array(
        'title' => '', //item title should be unique
        'icon' => '', //item icon should be unique
        'id' => '', //item icon should be unique
    ), $atts));
    //$clean_title = strtolower(urlencode($title)); //cleaning title
    //$clean_title =preg_replace('/[^a-z0-9]+/i', '_', $clean_title); //cleaning title
    $html = '<div class="panel panel-default">';
    $html .= '<div class="panel-heading" role="tab" id="head'.$id.'">';
    $html .= '<h4 class="panel-title">';
    $html .= '<a role="button" data-toggle="collapse" href="#' . $id . '" aria-expanded="true" aria-controls="'.$id.'">';
    $html .= '<i class="fa '. $icon .' "></i>' . $title . '<i class="fa  fa-caret-down"></i>' . '</a>';
    $html .= '</h4></div>'; //builds the email link and the pretty email icon
    $html .='<div id="'.$id.'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">';
    $html .= '<div class="panel-body">';
    $html .= do_shortcode($content) . '</div></div></div>';//key part here to allow shortcodes to function within the content
    return  $html;
}
add_shortcode( 'item', 'bsc_item' );




/* X-Frame-Options anti clickjacking */

add_action( 'send_headers', 'add_header_seguridad' );

function add_header_seguridad() {
    header( 'X-Content-Type-Options: nosniff' );
    header( 'X-Frame-Options: SAMEORIGIN' );
    header( 'X-XSS-Protection: 1;mode=block' );
}

?>