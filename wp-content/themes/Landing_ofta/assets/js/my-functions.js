jQuery(function ($) {

    //TOOLTIP
    $(function () {
        $('[data-toggle="tooltip1"]').tooltip();
        $('[data-toggle="tooltip2"]').tooltip();
        $('[data-toggle="tooltip3"]').tooltip();
        $('[data-toggle="tooltip4"]').tooltip();
    })

    //SEARCH
    $('.search-button').click(function(){
        $(this).parent().toggleClass('open');
    });

    //WOW
    var wow = new WOW();
    wow.init();

    //PLACEHOLDER SELECT
    $('select').change(function() {
        if ($(this).children('option:first-child').is(':selected')) {
            $(this).addClass('placeholder');
        } else {
            $(this).removeClass('placeholder');
        }
    });

    //SCROLLSPY
    //$('body').scrollspy({ target: '#bs-example-navbar-collapse-1' })

    //SCROLLANIMATION


    // Add smooth scrolling on all links inside the navbar
    $(".navbar-fixed-top a").on('click', function(event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {

            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $("html,body").animate(
                { scrollTop: $(hash).offset().top - $('.navbar-fixed-top').height() },
                1000
            );
            return false;
            e.preventDefault();

        } // End if

    });

    //OWL
    $('.owl-carousel').owlCarousel({
        items:1,
        dots:false,
        nav:true,
        merge:true,
        loop:true,
        video:true,
        lazyLoad:true,
        center:true,
    });

});