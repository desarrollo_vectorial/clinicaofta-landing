<?php

get_header();
wp_head();

?>
<?php
// VARS ACF INDEX
$video = get_field('shortcode_video', 'option');
$formulario = get_field('shortcode_formulario', 'option');
$facebook = get_field('facebook', 'option');
$youtube = get_field('youtube', 'option');
$instagram = get_field('instagram', 'option');
$background_ruta = get_field('background_ruta', 'option');
?>
<div id="slider">
    <?php echo do_shortcode($video); ?>
    <div id="formulario" class="animated slideInRight">
        <p><span>¡Déjanos tus datos</span> y te brindaremos una asesoría!</p>
        <?php echo do_shortcode($formulario); ?>
    </div>
</div>
<div id="nuestros-servicios">
    <div class="container-fluid">
        <div class="col-md-2">
            <h2>Nuestros<br><span>servicios</span></h2>
            <?php if( have_rows('tabs', 'option') ): ?>
            <?php $counter = 1;  //this sets up the counter starting at 0 ?>
            <ul class="nav nav-pills nav-stacked">
                <?php while( have_rows('tabs', 'option') ): the_row();
                    $active = get_sub_field('principal', 'option');
                    $titulo = get_sub_field('titulo', 'option');
                    ?>
                    <li class="<?php if( $active == 'si' ) { echo 'active' ;  } ?>">
                        <a href="#<?php echo $counter;?>" data-toggle="pill"><?php echo $titulo; ?></a>
                    </li>
                    <?php $counter++; // add one per row ?>
                <?php endwhile; wp_reset_postdata(); //END WHILE TABS?>
            </ul>
        </div>
        <div class="tab-content col-md-10">
            <?php $counter2 = 1;  //this sets up the counter starting at 0 ?>
            <?php while( have_rows('tabs', 'option') ): the_row();
                $active = get_sub_field('principal', 'option');
                $titulo = get_sub_field('titulo', 'option');
                $contenido = get_sub_field('contenido', 'option');
                $contenido = apply_filters('the_content', $contenido);
                $background = get_sub_field('background', 'option');
                ?>
                <div class="tab-pane wow  slideInRight <?php if( $active == 'si' ) { echo 'active' ;  } ?>" id="<?php echo $counter2;?>" data-wow-duration=".5s" data-wow-delay="0" style="background-image: url(<?php echo $background; ?>);">
                    <div class="row content">
                        <?php echo $contenido; ?>
                    </div>
                </div>
                <?php $counter2++; // add one per row ?>
            <?php endwhile; wp_reset_postdata(); //END WHILE TABS?>
            <?php endif; ?>
        </div><!-- tab content -->
    </div><!-- end of container -->
</div>
<div id="ruta-de-atencion" style="background-image: url(<?php echo $background_ruta; ?>);">
    <div class="ruta-title wow  slideInLeft">
        <p>Ruta de  atención<br><span>del paciente</span></p>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md 12">
                <div class="col-md-5"></div>
                <?php if( have_rows('pop-up', 'option') ): ?>
                    <?php $counterp = modal0;  //this sets up the counter starting at 0 ?>
                    <div class="col-md-7" id="content-popup">

                        <?php while( have_rows('pop-up', 'option') ): the_row();
                            $titulo = get_sub_field('titulo', 'option');
                            $image = get_sub_field('image', 'option');
                            $contenido = get_sub_field('contenido', 'option');
                            ?>

                            <div class="col-md-4 wow  slideInRight  <?php if ($counterp == 'modal3'){echo 'col-md-offset-2';}else{} ?>">
                                <!-- Button trigger modal -->
                                <a href="#<?php echo $counterp; ?>" type="button" data-toggle="modal" data-target="#<?php echo $counterp; ?>">
                                    <?php echo $titulo; ?>
                                    <img src="<?php echo $image; ?>" alt="" class="img-responsive">
                                </a>

                                <!-- Modal -->
                                <div class="modal fade" id="<?php echo $counterp; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $counterp; ?>Label">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <?php echo $contenido; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $counterp++; // add one per row ?>
                        <?php endwhile;  //ENDWHILE ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div id="por-que">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <?php if( have_rows('pasos', 'option') ): ?>
                    <h2>¿Por qué escoger la Clínica de Oftalmología de Cali?</h2>
                    <?php while( have_rows('pasos', 'option') ): the_row();
                        $image = get_sub_field('imagen_porque', 'option');
                        $texto = get_sub_field('texto', 'option');
                        ?>
                        <div class="col-md-4 wow  slideInRight">
                            <img src="<?php echo $image; ?>" alt="" class="img-responsive">
                            <?php echo $texto; ?>
                        </div>
                    <?php endwhile;  //ENDWHILE?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div id="historias">
    <div class="title">
        <p><span>Historias de éxito</span> de nuestros pacientes</p>
    </div>
    <?php if( have_rows('testimonios', 'option') ): ?>
    <div class="owl-carousel owl-theme">
        <?php while( have_rows('testimonios', 'option') ): the_row();
            $id = get_sub_field('id_del_video', 'option');
            $testimonio = get_sub_field('texto_del_testimonio', 'option');
            ?>
        <div class="item">
            <div class="video">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item " src="//www.youtube.com/embed/<?php echo $id; ?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
                <div class="testimonio">
                    <p><?php echo $testimonio; ?></p>
                </div>
            </div>
        </div>
        <?php endwhile;  //ENDWHILE?>
    </div>
    <?php endif; ?>
</div>



<?php

wp_footer();
get_footer();

?>


