<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

define('HTTP_HOST', $_SERVER["HTTP_HOST"]);
define('LOCAL_DOMAIN', 'clinicaofta.landing.local');
define('DEV_LOCAL_VIEW', 'clinicaofta.landing.192.168.0.40.xip.io');
define('DEV_DOMAIN', 'clinicaofta-landing.vectorial.co');
define('PROD_DOMAIN', '');
define('PROD_DOMAIN2', '');



// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //

switch (HTTP_HOST) {
    case LOCAL_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://clinicaofta.landing.local/');
        define('WP_SITEURL','http://clinicaofta.landing.local/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'dev_ofta_landing');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case DEV_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://clinicaofta-landing.vectorial.co/');
        define('WP_SITEURL','http://clinicaofta-landing.vectorial.co/');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'ofta_landing');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'vectorial$_$');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;

    case DEV_LOCAL_VIEW:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','http://clinicaofta.landing.192.168.0.40.xip.io');
        define('WP_SITEURL','http://clinicaofta.landing.192.168.0.40.xip.io');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'ofta_landing');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'root');
        /** Tu contraseña de MySQL */
        if ( substr(PHP_OS, 0, 3) === 'WIN' ) {
            define('DB_PASSWORD', '');
        }
        else {
            define('DB_PASSWORD', 'root');
        }
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;
    case PROD_DOMAIN:
        /** El nombre de tu base de datos de WordPress */
        define('WP_HOME','');
        define('WP_SITEURL','');
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'hematoon_portal');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'hematoon_user_portal');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'xv]u86hJ2W)G');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', 'localhost');
        break;

    case PROD_DOMAIN2:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', 'webcrc_w3bs1t3');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', 'webcrc_w3bs1t3');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', 'crcv4ll3w3bs1t3');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '127.0.0.1');
        break;
    default:
        /** El nombre de tu base de datos de WordPress */
        define('DB_NAME', '');
        /** Tu nombre de usuario de MySQL */
        define('DB_USER', '');
        /** Tu contraseña de MySQL */
        define('DB_PASSWORD', '');
        /** Host de MySQL (es muy probable que no necesites cambiarlo) */
        define('DB_HOST', '');
        break;
}
/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8mb4');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'H=dQN !;AHvO_NUl]{X[ )fSV!^299o&?KMuz$S_Afy{^mvc;gM%MwGL[UuaMi?k');
define('SECURE_AUTH_KEY', 'o#we$Rd.%-XVpb`?h?c)3`l?k/)B3_U DA8n}sQf:$Y`@eo{-?ghe##mk}gI]M1v');
define('LOGGED_IN_KEY', '(0qw+JTldP.o~GOQ7{3GgM8_au43-K4M#k}hPGW--j3eRpymUieNZVz{9fQ6-`d<');
define('NONCE_KEY', 'hxE]-EFy![M$$f+K0}@}tIlogCDMXo;a 6BsxY.2k*55{M0 z1;chvEA!(b<nKA6');
define('AUTH_SALT', '))*Us!AK,&?kR%c77jo.3Y53D(rt){X {Yz#rK-D_oWip_OlvJ<giVUP0jBgc};0');
define('SECURE_AUTH_SALT', ']!~-uO_gF)iU|ZL+Uhy ~BM||u:R%n-b.k#=9@X/9Eig.Zja.Gdb~R&U:N,:79n:');
define('LOGGED_IN_SALT', '*D!xg<3N&8`1=,3=7_;Sen2`v{C:jTl[.PT`Ca(m`#,JQI4uF^srcwh?+.AQ)+!2');
define('NONCE_SALT', '):U:+TB?=BQI]A?<~U}14Wz;B,Kc8?iO%`RGdd=s:Zg4Q9VeX}BoM-?A}Y[Zh6A1');
//eliminar p y br contact-form 7
define('WPCF7_AUTOP', false );

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'lpofta_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

